import org.apache.commons.configuration2.PropertiesConfiguration;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.junit.jupiter.api.BeforeAll;

import java.io.IOException;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;

public class BaseTest
{
    @BeforeAll
    public static void setUp() throws IOException, ConfigurationException
    {
        // In a more massive test framework I'd like to add these lines as dependency injection. For now it's somewhat hard coded to save time.
        PropertiesConfiguration props = ConfigLoader.load();
        RestAssured.baseURI = props.getString("base.uri");
        RestAssured.requestSpecification = new RequestSpecBuilder().addParam("api_key", props.getString("api.key")).build();
    }
}
