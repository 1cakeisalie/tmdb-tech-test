import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

public class SearchTests extends BaseTest
{
    @Test
    @Disabled("Stub test")
    void trimSpacesInSearchQuery()
    {
        /*
             TODO: Check that search queries trim blank spaces.
         */
    }

    @Test
    @Disabled("Stub test")
    void sqlInjectionInSearchQuery()
    {
        /*
             TODO: Make sure the user can't inject SQL queries into a search.
         */
    }

    @Test
    @Disabled("Stub test")
    void searchResultCount()
    {
        /*
             TODO: Don't rely on the pagination count in the JSON to be correct.
             Double check that there are actually X number of pages
         */
    }

    @Test
    @Disabled("Stub test")
    void emptySearchQuery()
    {
        /*
             TODO: An empty search query should return proper messaging.
             I should not get messaging that the system attempted to search if the query was empty.
             This would indicate a waste of system resources for empty requests.
         */
    }
}
