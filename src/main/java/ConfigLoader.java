import org.apache.commons.configuration2.PropertiesConfiguration;
import org.apache.commons.configuration2.builder.fluent.Configurations;
import org.apache.commons.configuration2.ex.ConfigurationException;

import java.io.File;
import java.io.IOException;

public class ConfigLoader
{
    private static final File configFile = new File("config.properties");
    private static Configurations configs = new Configurations();

    public static PropertiesConfiguration load() throws ConfigurationException, IOException
    {
        return configs.properties(configFile);
    }
}
