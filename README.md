## Tech Assessment Project

This project tests against https://www.themoviedb.org API. 

It contains 12 tests, 4 of which are fully written out and can be executed.

To run:

 - Install Maven 3
 - Install JDK8
 - At the project folder level, run `mvn install`
 - Make sure that all dependencies are resolved in the pom.xml. If you run into problems, reindex your local maven repository and try again. 
 Also make sure that Junit5 is in your classpath.
 - To execute the tests run `mvn test`